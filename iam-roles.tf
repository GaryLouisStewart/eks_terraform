#
# EKS Master nodes AWS IAM policys
#

resource "aws_iam_role" "eks-test-1-master" {
  name = "${var.cluster-name}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "role-policy-attachment-eks-test-1-master" {
  role       = "${aws_iam_role.eks-test-Master.name}"
  count      = "${length(var.master-node-policy-arn)}"
  policy_arn = "${var.master-node-policy-arn[count.index]}"
}

#
# EKS worker node AWS IAM policy
#

resource "aws_iam_role" "eks-test-1-worker" {
  name = "${var.cluster-name}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "role-policy-attachment-eks-test-1-worker" {
  role       = "${aws_iam_role.eks-test-1-worker.name}"
  count      = "${length(var.worker-node-policy-arn)}"
  policy_arn = "${var.worker-node-policy-arn[count.index]}"
}


resource "aws_iam_instance_profile" "eks-test-1" {
  name = "${var.cluster-name}"
  role = "${aws_iam_role.eks-test-1-worker.name}"
}

resource "aws_iam_role" "efk-log-forwarding-eks" {
  name = "efk-logging-${var.tags_all["Name"]}"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "logs:DescribeLogGroups",
                "logs:DescribeLogStreams",
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*",
            "Effect": "Allow"
        }
    ]
}
EOF

  tags = "${var.tags_all}"
}

resource "aws_iam_role" "lambda-basic-execution" {
  name = "lambda-execution-${var.tags_all["Name"]}"

  assume_role_policy = <<EOF
{
   "Version": "2012-10-17",
   "Statement": [
   {
     "Effect": "Allow",
     "Principal": {
        "Service": "lambda.amazonaws.com"
     },
   "Action": "sts:AssumeRole"
   }
 ]
}
EOF

  tags = "${var.tags_all}"
}

resource "aws_iam_role_policy_attachment" "lambda-basic-execution" {
  policy_arn = "arn:aws:iam::aws:policy/AWSLambdaExecute"
  role       = "${aws_iam_role.eks-test-1.name}"
}

resource "aws_iam_role_policy_attachment" "efk-cloudwatch-logs" {
  policy_arn = "arn:aws:iam::aws:policy/AWSLambdaExecute"
  role       = "${aws_iam_role.eks-test-1.name}"
}
