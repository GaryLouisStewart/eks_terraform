# this is the actual eks master that will be responsible for controlling our kubernetes nodes.
resource "aws_eks_cluster" "eks-test-1" {
  name            = "${var.cluster-name}"
  role_arn        = "${aws_iam_role.eks-test-1.arn}"

  vpc_config {
    security_group_ids = ["${aws_security_group.eks-test-1.id}"]
    subnet_ids         = ["${aws_subnet.eks-test-1.*.id}"]
  }

  depends_on = [
    "aws_iam_role_policy_attachment.eks-test-1-AmazonEKSClusterPolicy",
    "aws_iam_role_policy_attachment.eks-test-1-AmazonEKSServicePolicy",
  ]
}
