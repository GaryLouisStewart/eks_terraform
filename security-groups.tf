#
# Allow communication from our local workstation into the VPC via the MASTER NODES
#
resource "aws_security_group" "eks-test-1" {
  name        = "${var.cluster-name}"
  description = "Cluster communication with worker nodes"
  vpc_id      = "${aws_vpc.eks-test-1.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.cluster-name}"
  }
}

# OPTIONAL: Allow inbound traffic from your local workstation external IP
#           to the Kubernetes. You will need to replace A.B.C.D below with
#           your real IP. Services like icanhazip.com can help you find this.
resource "aws_security_group_rule" "${var.cluster-name}-ingress-workstation-https" {
  cidr_blocks       = ["A.B.C.D/32"]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.eks-test-1.id}"
  to_port           = 443
  type              = "ingress"
}
#
# The groups below networking control access to the worker nodes from the PODS, kublets and between each other
#

resource "aws_security_group" "eks-test-1" {
  name        = "${var.cluster-name}"
  description = "Security group for all nodes in the cluster"
  vpc_id      = "${aws_vpc.eks-test-1.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${
    map(
     "Name", "${var.cluster-name}",
     "kubernetes.io/cluster/${var.cluster-name}", "owned",
    )
  }"
}

resource "aws_security_group_rule" "eks-test-1-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.eks-test-1.id}"
  source_security_group_id = "${aws_security_group.eks-test-1.id}"
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "eks-test-1-ingress-cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.eks-test-1.id}"
  source_security_group_id = "${aws_security_group.eks-test-1.id}"
  to_port                  = 65535
  type                     = "ingress"
}

#
# Allow the worker nodes to access the kubernetes master node
#

resource "aws_security_group_rule" "eks-test-1-ingress-node-https" {
  description              = "Allow pods to communicate with the cluster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.eks-test-1.id}"
  source_security_group_id = "${aws_security_group.eks-test-1.id}"
  to_port                  = 443
  type                     = "ingress"
}
