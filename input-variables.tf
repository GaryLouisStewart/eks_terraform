# Input variables for configuring the cluster. These are all mandatory and need to be set.
#	 **NOTE** please leave the access_key and secret_key variables blank as the script(WIP)
###			  will pass these in during runtime.


variable "access_key" {
  default = ""
  type = "string"
  description = "AWS Acces key set the default to nothing and pass in during runtime"
}

variable "secret_key" {
  default = ""
  type = "string"
  description = "AWS secret key set the default to nothing and pass in during runtime"
}

variable "region" {
  default = "eu-west-1"
  description = "AWS region you want to deploy to. Note that for EKS we can only us US-east or eu-west"
}

variable "cluster-name" {
  default = "eks-test-1"
  type    = "string"
  description = "Name of the EKS cluster"
}

variable "aws-ec2-instance-size" {
  default = "t3.micro"
  type    = "string"
  description = "Size of the EC2 instances"
}

variable "worker-count" {
  default = "2"
  type = "string"
  description = "Number of worker nodes to run in our kubernetes cluster"
}

variable "worker-node-image" {
  default = "amazon-eks-node-*"
  type = "string"
  description = "EKS worker node image to use"
}

variable "tf-state-bucket" {
  default = "CHANGEME"
  type = "string"
  description = "The location of the s3 bucket for terraform state"
}

variable "tf-state-key" {
  default = "CHANGEME"
  type = "string"
  description = "The location of the tf-state key file (terraform.tfstate) file"
}


variable "worker-node-iam-policy-arn" {
  type = "list"
  description = "The arns to be passed in for the eks-worker-nodes"
  default = [ "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy", "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy", "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"]
}

variable "master-node-iam-policy-arn" {
  type = "list"
  description = "the arns to be passed in for the eks-master-nodes"
  default = ["arn:aws:iam::aws:policy/AmazonEKSClusterPolicy", "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"]
}

variable "tags_all" {
  type = "map"

  default = {
    Name = "eks-cluster"
	Department = "Application Development"
	Environment = "DEV"
	client = "example.ltd"
	date = "2019-01-12"
  }
}