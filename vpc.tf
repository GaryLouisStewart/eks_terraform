data "aws_availability_zones" "available" {}

resource "aws_vpc" "eks-test-1" {
  cidr_block = "10.0.0.0/16"

  tags = "${
    map(
     "Name", "eks-test-1",
     "kubernetes.io/cluster/${var.cluster-name}", "shared",
    )
  }"
}