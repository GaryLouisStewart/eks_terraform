#
# kubernetes worker node setup
#
data "aws_region" "current" {}

data "aws_ami" "eks-worker" {
  filter {
    name   = "${var.cluster-name}"
    values = "${var.worker-node-image}"
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon EKS AMI Account ID
}

#
# EKS Worker node launch configuration
#
locals {
  eks-test-1-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.eks-test-1.endpoint}' --b64-cluster-ca '${aws_eks_cluster.demo.certificate_authority.0.data}' '${var.cluster-name}'
USERDATA
}

resource "aws_launch_configuration" "eks-test-1" {
  associate_public_ip_address = true
  iam_instance_profile        = "${aws_iam_instance_profile.eks-test-1.name}"
  image_id                    = "${data.aws_ami.eks-worker.id}"
  instance_type               = "${var.aws-ec2-instance-size}"
  name_prefix                 = "${var.cluster-name}"
  security_groups             = ["${aws_security_group.eks-test-1.id}"]
  user_data_base64            = "${base64encode(local.eks-test-1-userdata)}"

  lifecycle {
    create_before_destroy = true
  }
}

# autoscaling group for the worker nodes

resource "aws_autoscaling_group" "eks-test-1" {
  desired_capacity     = "${var.worker-count}"
  launch_configuration = "${aws_launch_configuration.eks-test-1.id}"
  max_size             = "${var.worker-count}"
  min_size             = 1
  name                 = "eks-test-1"
  vpc_zone_identifier  = ["${aws_subnet.eks-test-1.*.id}"]

  tag {
    key                 = "Name"
    value               = "${var.cluster-name}"
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${var.cluster-name}"
    value               = "owned"
    propagate_at_launch = true
  }