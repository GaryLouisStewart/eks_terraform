**Terraform EKS setup**

The following is intended to be a utility to get a basic EKS cluster setup on amazon AWS using terraform. it includes everything you'll need in order to get up and running.'

---

## Instructions- environment setup
1. Set your AWS credentials the 'AWS_SECRET_ACCESS_KEY' and 'AWS_ACCESS_KEY_ID' variables in the scripts/AWS_KEYS.sh script
2. make sure to set all variables in the input-variables.tf file

## Instructions- terraform plan

The following steps can be taken in order to run tfplan
1. run the scripts/tf-action.sh shell script with the command './scripts/tfaction.sh plan'
2. you will now get output of a terraform plan, log files are stored in the logs directory and date + timestamped
3. note that tfplan tells you what it is going to do in our environment before it sets up your infrastructure
---

## Instructions- terraform apply

The following steps can be taken in order to run tfapply

1. run the scripts/tf-action.sh shell script with the command './scripts/tf-action.sh tfapply'
2. You will now get output for a terraform apply, which will setup all the required resources on AWS.
3. tfapply should setup all the required resources and our EKS cluster, note that the EKS cluster will take a while to become available
---

## Instructions- terraform destroy

The following steps can be taken in order to run tfdestroy, note that this should only ever be run if you are sure of what you are doing, it will remove all infrastructure. 

1. run the scripts/tfaction.sh shell script with the command './scripts/tf-action.sh tfdestroy' 
2. this will run a tfplan with the -destroy flag and output the log file to our logs folder
3. this script should only ever be run when decomissioning an environment as it will destroy all infrastructure
---

## Instructions- terraform output

The following steps can be taken in order to run tfoutput, note this can be run after a tfapply to get information such as the kubeconfig for our cluster

1. run the scripts/tfaction.sh shell script with the command './scripts/tf-action.sh tfoutput <resource>'
2. this is the only command in our script which expects an extra parameter where the extra parameter is the output of the variable you want for example './scripts/tf-action.sh tfoutput kubeconfig'
3. if you want the output of a resource add it into the 'outputs.tf' file