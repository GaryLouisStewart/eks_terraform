# create private s3 bucket for our cluster that is encrypted with a private kms key
## create iam role with policy access to s3 for terraform state files
### set default backed to use these created resources to keep state.

# create kms key to encrypt bucket (expiry after a year)

resource "aws_kms_key" "eks-test-1" {
  name = "${var.cluster-name}"			
  description = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 365
}

# create s3 bucket, enable versioning by default

resource "aws_s3_bucket" "eks-test-1" {
  bucket = "${var.tf-state-bucket}"
  region = "${var.region}"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "${aws_kms_key.eks-test-1.arn}"
        sse_algorithm     = "aws:kms"
      }

  versioning {
    enabled = true
  }

  tags {
    Name = "${var.cluster-name}"
  }
}

# setup iam role with specific access for this bucket

resource "aws_iam_role" "eks-test-1-s3" {
  name = "eks-test-1-s3-state"
  bucket = "${eks-test-1.id}"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:ListBucket",
      "Resource": "arn:aws:s3:::{$var.tf-state-bucket}"
    },
    {
      "Effect": "Allow",
      "Action": ["s3:GetObject", "s3:PutObject"],
      "Resource": "arn:aws:s3:::eks-test-1/terraform.tfstate"
    }
  ]
}
POLICY
}

# finally set the default terraform backend to point to our s3 bucket

terraform {
  backend "s3" {
    bucket = "${var.tf-state-bucket}"
    key    = "${var.tf-state-key}"
    region = "${var.region}"
	kms_key_id = "${aws_kms_key.eks-test-1.arn}"
  }
}
