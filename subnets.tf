resource "aws_subnet" "eks-test-1" {
  count = 2

  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block        = "10.0.${count.index}.0/24"
  vpc_id            = "${aws_vpc.eks-test-1.id}"

  tags = "${
    map(
     "Name", "eks-test-1-node",
     "kubernetes.io/cluster/${var.cluster-name}", "shared",
    )
  }"
}