resource "aws_route_table" "eks-test-1" {
  vpc_id = "${aws_vpc.eks-test-1.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.eks-test-1.id}"
  }
}

resource "aws_route_table_association" "eks-test-1" {
  count = 2

  subnet_id      = "${aws_subnet.eks-test-1.*.id[count.index]}"
  route_table_id = "${aws_route_table.eks-test-1.id}"
}