resource "aws_internet_gateway" "eks-test-1" {
  vpc_id = "${aws_vpc.eks-test-1.id}"

  tags {
    Name = "eks-test-1"
  }
}